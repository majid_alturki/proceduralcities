    č          2019.4.16f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                      \       ŕyŻ     `                                                                                                                                                                               ŕyŻ                                                                                    UIChannelListItem     using UnityEngine;
using TNet;

/// <summary>
/// Helper class attached to the channel list prefab that makes it easy to update channel-related information.
/// It also reacts to the click event, allowing to join an existing game.
/// </summary>

[RequireComponent(typeof(UIButton))]
public class UIChannelListItem : MonoBehaviour
{
	public UILabel titleLabel;
	public UILabel playerLabel;
	public UILabel descriptionLabel;

	// These values are set by the UIChannelList class
	[HideInInspector] public int id;
	[HideInInspector] public bool isValid = false;

	string mSceneName;
	UIButton mButton;
	Color mDefaultColor = Color.white;
	int mServerVersion = 0;
	bool mPass = false;

	void OnEnable ()
	{
		TNManager.onSetChannelData += OnNetworkSetChannelData;
	}

	void OnDisable () 
	{ 
		TNManager.onSetChannelData -= OnNetworkSetChannelData;
	}

	void Awake ()
	{
		mButton = GetComponent<UIButton>();
		mDefaultColor = descriptionLabel.color;
	}

	/// <summary>
	/// Join the channel.
	/// </summary>

	void OnClick ()
	{
		if (TNManager.isConnected)
		{
			if (mPass)
			{
				// If there is a password, show the password dialog
				UIPasswordWindow.Show(id, mSceneName);
			}
			else
			{
				// Join the channel
				mButton.isEnabled = false;
				GameManager.gameType = GameManager.GameType.Multiplayer;
				TNManager.JoinChannel(id, mSceneName, false, 1, null);
			}
		}
	}

	/// <summary>
	/// Set the visible channel data.
	/// When modifying this function, be sure to modify GameManager.UpdateChannelData as well.
	/// </summary>

	public void Set (Channel.Info info)
	{
		mPass = info.hasPassword;
		Set (info.data);
		mSceneName = info.level;
		playerLabel.text = info.players + "/" + info.limit;

		if (mServerVersion == UIVersion.buildID) 
		{
			mButton.isEnabled = (info.players < info.limit);
		}
		else
		{
			mButton.isEnabled = false;
		}

		isValid = true;
	}

	/// <summary>
	/// Sets any DataNode information for the channel.  Servers may periodically update this information
	/// with the SetChannelData command.
	/// </summary>

	void Set (DataNode node)
	{
		titleLabel.text = node.GetChild<string> ("Game Name");
		mServerVersion = node.GetChild<int> ("Build");

		if (mServerVersion == UIVersion.buildID) {
			// Progress percentage comes last
			descriptionLabel.text = string.Format ("{0} - {1}", titleLabel.text, node.GetChild<string> ("Progress"));
			descriptionLabel.color = mPass ? Color.red : mDefaultColor;
		} else {
			// Version mismatch
			descriptionLabel.text = string.Format (Localization.Get ("Version"), mServerVersion);
			descriptionLabel.color = Color.red;
		}
	}

	/// <summary>
	/// When a SetChannelData event is triggered, update the listbox with the new information.
	/// </summary>
	
	void OnNetworkSetChannelData (Channel ch, string path, DataNode node)
	{
		Set (node);
	}

}
                         UIChannelListItem       