﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralBuilding : MonoBehaviour
{
    [SerializeField]
    private GameObject roomPrefab;

    [SerializeField]
    private GameObject roofPrefab;

    [SerializeField]
    private float unitCellSize;

    [SerializeField]
    private Vector3 roofOffset;

    [SerializeField]
    private Material glowMat;

    private static ProceduralBuilding _instance;

    Floor[] floors;

    public static ProceduralBuilding Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<ProceduralBuilding>();
            return _instance;
        }
    }

    GameObject MakeBuilding(int width, int height, int floorsCount, bool hasRoof)
    {
        var floorCount = 0;
        foreach (Floor floor in floors)
        {
            Room[,] rooms = new Room[width, height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    rooms[i, j] = new Room(new Vector2(i * unitCellSize, j * unitCellSize),

                        floorCount == floors.Length - 1 ? hasRoof : false);
                }
            }
            floors[floorCount] = new Floor(rooms, floorCount++);
        }
        return Render(width, height);
    }

    private GameObject Render(int width, int height)
    {
        GameObject parent = new GameObject();
        var RoomParent = Instantiate(parent, transform.position, Quaternion.identity);
        foreach (Floor floor in floors)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Room room = floor.Rooms[i, j];
                    var roomInstance = Instantiate(roomPrefab, new Vector3(room.Position.x, floor.FloorNumber * roomPrefab.transform.localScale.x, room.Position.y), Quaternion.Euler(0, 0,0));
                    roomInstance.transform.parent = RoomParent.transform;
                    AddWindows(roomInstance.transform.GetComponentsInChildren<MeshRenderer>());
                    

                    if (room.hasRoof)
                    {
                        var roof = Instantiate(roofPrefab, new Vector3(room.Position.x + roofOffset.x, floor.FloorNumber * roomPrefab.transform.localScale.x + roofOffset.y, room.Position.y + roofOffset.z),
                            roofPrefab.transform.rotation);
                        roof.transform.parent = RoomParent.transform;
                    }

                }
            }
        }
        RoomParent.AddComponent<BoxCollider>();
        return RoomParent;
    }

    void AddWindows(MeshRenderer[] childsRenderer)
    {
        foreach (MeshRenderer r in childsRenderer)
        {
            if (Random.value > 0.5f)
            {
                r.material = glowMat;
                r.material.SetColor("_EmissionColor", new Color(Random.Range(0,255), Random.Range(0, 255), Random.Range(0, 255), 1) * 0.005f);
            }
        }
    }

    public GameObject GetBuilding(int width, int height, int numOfFloors, bool hasRoof)
    {
        floors = new Floor[numOfFloors];
        return MakeBuilding(width, height, numOfFloors, hasRoof);
    }

}