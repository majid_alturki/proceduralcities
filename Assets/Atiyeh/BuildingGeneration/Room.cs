﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    private Vector2 position;

    public bool hasRoof;

    public Vector2 Position
    {
        get => position;
        set => position = value;
    }


    public Room(Vector2 position,  bool hasRoof = false)
    {
        this.Position = position;
        this.hasRoof = hasRoof;
    }

}
